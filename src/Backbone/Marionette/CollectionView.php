<?php
namespace Backbone\Marionette;

class CollectionView extends ItemView
{
    public $collection;

    public function __construct($options)
    {
        parent::__construct($options);

        $this->collection = empty($options['collection']) ? [] : $options['collection'];
        $this->childView = $options['childView'];
    }

    public function render()
    {
        $this->deleteChildren($this->el);

        //$el = $this->el->appendChild($this->el->ownerDocument->createElement($this->tagName));

        foreach ($this->collection as $model) {
            $view = new $this->childView([
                'model'=>$model,
            ]);

            $view->setElement($this->el->appendChild($this->el->ownerDocument->createElement($view->tagName)));

            $view->render();
        }

        return $this;
    }
}
