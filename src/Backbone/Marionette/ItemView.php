<?php
namespace Backbone\Marionette;

use Backbone\View;
use Backbone\Marionette;

class ItemView extends View
{
    public $template;

    public function __construct($options)
    {
        parent::__construct($options);
        $this->template = empty($options['template']) ? $this->template : $options['template'];
    }

    public function render()
    {
        $this->deleteChildren($this->el);
        $html = $this->el->ownerDocument->createDocumentFragment();

        $html->appendXML(
            Marionette::$Renderer->render(
                $this->template,
                array_merge(
                    $this->options,
                    $this->serializeData()
                )
            )
        );

        $this->el->appendChild($html);

        return $this;
    }

    protected function deleteChildren($node) {
        while (isset($node->firstChild)) {
            $this->deleteChildren($node->firstChild);
            $node->removeChild($node->firstChild);
        }
    }

    public function serializeData()
    {
        return !empty($this->model) ? $this->model
                                    : (!empty($this->collection) ? ['items'=>$this->collection] : []);
    }
}
