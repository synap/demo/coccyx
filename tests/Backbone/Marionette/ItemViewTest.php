<?php
namespace Backbone\Marionette;

use PHPUnit_Framework_TestCase;
use DOMDocument;
use Backbone\Marionette;

class ItemViewTest extends PHPUnit_Framework_TestCase
{
    private $view;

    public function setUp()
    {
    }

    public function testRenderWithCallback()
    {
        $callback = function(){return '<h1>foo</h1>';};

        $renderer = $this->getMock('Renderer', array('render'));

        $renderer
            ->expects($this->once())
            ->method('render')
            ->with($this->equalTo($callback))
            ->willReturn("<h1>foo</h1>");

        Marionette::$Renderer = $renderer;

        $dom = new DOMDocument();
        $dom->loadXML('<root />');

        $view = new ItemView([
            'template' => $callback,
            'el' => $dom->documentElement
        ]);

        $view->render();

        $this->assertEquals(
            $view->el->c14n(),
            '<root><h1>foo</h1></root>'
        );
    }

    public function testRenderWithId()
    {
        $renderer = $this->getMock('Renderer', array('render'));

        $renderer
            ->expects($this->once())
            ->method('render')
            ->with($this->equalTo('button'))
            ->willReturn("<h1>foo</h1>");

        Marionette::$Renderer = $renderer;

        $dom = new DOMDocument();
        $dom->loadXML('<root />');

        $view = new ItemView([
            'template' => 'button',
            'collection' => [],
            'el'=>$dom->documentElement
        ]);

        $view->render();

        $this->assertEquals(
            $view->el->c14n(),
            '<root><h1>foo</h1></root>'
        );
    }
}
