<?php
namespace Backbone\Marionette;

use PHPUnit_Framework_TestCase;
use DOMDocument;
use Backbone\Marionette;

class RegionTest extends PHPUnit_Framework_TestCase
{
    private $view;

    public function setUp()
    {
    }

    public function testShow()
    {
        $view = $this->getMock('Backbone\View', array('render'));

        $view
            ->expects($this->once())
            ->method('render');

        $dom = new DOMDocument();
        $dom->loadXML('<root><region /></root>');

        $region = new Region([
            'el'=>$dom->getElementsByTagName('region')->item(0)
        ]);

        $region->show($view);

        $frag = $dom->createDocumentFragment();
        $frag->appendXML('<h1>foo</h1>');

        $view->el->appendChild($frag);


        $this->assertEquals(
            $dom->c14n(),
            '<root><region><div><h1>foo</h1></div></region></root>'
        );
    }

}
