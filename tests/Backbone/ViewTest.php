<?php

namespace Backbone;

use PHPUnit_Framework_TestCase;
use DOMDocument;

class ViewTest extends PHPUnit_Framework_TestCase
{
    private $view;

    public function setUp()
    {
        $dom = new DOMDocument();
        $dom->loadXML('<root />');

        $this->view = new View([
            'el'=>$dom->getElementsByTagName('root')->item(0)
        ]);
    }

    public function testEl()
    {
        $this->assertEquals(
            $this->view->el->c14n(),
            '<root></root>'
        );
    }
}
